var compareApp = angular.module('compare', ['compareFilter']);

angular.module('compareFilter', []).filter('removeSelected', function() {
	return function(laptops, laptopA, laptopB) {
		var list = [{}];
		list.pop();
		for (x = 0; x < laptops.length; x++) {
			if (laptops[x] != laptopA && laptops[x] != laptopB) {
				list.push(laptops[x]);
			}
		}
		
		return list;		
	};
});

compareApp.directive('comparison', function() {
	        return {
	            restrict: 'E',
	            replace: true,
	            templateUrl: 'directive/comparison.html',
	            scope: {
	                laptop: '=laptop'
	            }
	        }
	    });

compareApp.controller('compareCtrl', function ($scope, $filter) {
	
	$scope.laptops = [
	               {
	                   'name' : 'Acer Aspire E1-572',
	                   'price' : 399,
	                   'os': 'Windows 8.1',
	                   'weight' : 2.193,
	                   'processor' : 'Intel Core i5-4200U',
	                   'processor_speed' : 1.6,
	                   'ram' : 4,
	                   'storage' : 'Spinning HD 5400',
	                   'capacity' : 500
	               },
	               {
	                   'name' : 'Acer Aspire S3-392G',
	                   'price' : 899.99,
	                   'os': 'Windows 8.1',
	                   'weight' : 1.618,
	                   'processor' : 'Intel Core i5-4200U',
	                   'processor_speed' : 1.6,
	                   'ram' : 4,
	                   'storage' : 'Spinning HD 5400',
	                   'capacity' : 500
	               },
	               {
	                   'name' : 'Acer Aspire V5-573',
	                   'price' : 599.95,
	                   'os': 'Windows 8.1',
	                   'weight' : 2.045,
	                   'processor' : 'Intel Core i7-4500U',
	                   'processor_speed' : 1.8,
	                   'ram' : 8,
	                   'storage' : 'Spinning HD 5400',
	                   'capacity' : 1000
	               },
	               {
	                   'name' : 'Acer V5-552',
	                   'price' : 479.99,
	                   'os': 'Windows 8.1',
	                   'weight' : 2,
	                   'processor' : 'AMD A10-5757M',
	                   'processor_speed' : 2.1,
	                   'ram' : 6,
	                   'storage' : 'Spinning HD 5400',
	                   'capacity' : 1000
	               },
	               {
	                   'name' : 'Apple MacBook Air 11-inch (2013)',
	                   'price' : 585.00,
	                   'os': 'OSX 10.8.4',
	                   'weight' : 1.079,
	                   'processor' : 'Intel Core i5-4250U',
	                   'processor_speed' : 1.3,
	                   'ram' : 4,
	                   'storage' : 'SSD',
	                   'capacity' : 128
	               },
	               {
	                   'name' : 'Apple MacBook Air 13-inch (2013)',
	                   'price' : 669.99,
	                   'os': 'OSX 10.8.4',
	                   'weight' : 1.328,
	                   'processor' : 'Intel Core i5-4250U',
	                   'processor_speed' : 1.3,
	                   'ram' : 4,
	                   'storage' : 'SSD',
	                   'capacity' : 128
	               },
	               {
	                   'name' : 'Apple Macbook Pro Retina 13-inch (2013)',
	                   'price' : 856.39,
	                   'os': 'Mac OS 10.9',
	                   'weight' : 1.566,
	                   'processor' : 'Intel Core i5-4258U',
	                   'processor_speed' : 2.4,
	                   'ram' : 4,
	                   'storage' : 'SSD',
	                   'capacity' : 128
	               },
	               {
	                   'name' : 'Apple Macbook Pro Retina 15-inch (2013)',
	                   'price' : 1338.00,
	                   'os': 'Mac OS 10.9',
	                   'weight' : 1.997,
	                   'processor' : 'Intel Core i7-4750HQ',
	                   'processor_speed' : 2,
	                   'ram' : 8,
	                   'storage' : 'SSD',
	                   'capacity' : 256
	               }
	           ];
		
	
});